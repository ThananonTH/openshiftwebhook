<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class WebhookController extends Controller{
    public function handle(Request $request){

        //Config Base URI for API
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://test-fm-wsgw.intra.ais',
            'verify' => false,
        ]);

        //Put the received webhook ($request) to webhook.log
        Log::channel('webhook')->info($request);
        Log::channel('webhook')->info(count($request['alerts']));

        //For loop to access the fields of each alert in webhook
        //Since a webhook may have multiple alerts in the indexed array, a set of parameters from each alert will be sent in each loop 
        for($i = 0; $i < count($request['alerts']); $i++){
            //Create the set of parameters from mapped fields of index-$i alert 
            Log::channel('webhook')->info('Loop '.$i);
            $input['module'] = 'StandardFormat';
            $input['amoName'] = '';
            $input['alarmName'] = '';
            $input['description'] = '';
            $input['node'] = '';

            if(array_key_exists('node',$request['alerts'][$i]['labels']))
                $input['amoName'] = $request['alerts'][$i]['labels']['node'];
            else if(array_key_exists('namespace',$request['alerts'][$i]['labels']))
                $input['amoName'] = $request['alerts'][$i]['labels']['namespace'];

            if(array_key_exists('alertname',$request['alerts'][$i]['labels']))
                $input['alarmName'] = $request['alerts'][$i]['labels']['alertname'];
                
            if(array_key_exists('description',$request['alerts'][$i]['annotations']))
                $input['description'] = $request['alerts'][$i]['annotations']['description'];

            if(array_key_exists('pod',$request['alerts'][$i]['labels']))
                $input['node'] = $request['alerts'][$i]['labels']['pod'];

            $input['mcZone'] = '';
            $input['systemName'] = 'OCP';
            $input['emsName'] = 'viking904.odin.valhalla.intra.ais';
            $input['emsIp'] = '10.183.146.5';
            $input['siteCode'] = '804';

            $input['severity'] = '';
            if(array_key_exists('severity',$request['alerts'][$i]['labels'])){
                if($request['alerts'][$i]['status'] == 'resolved')
                    $input['severity'] = 'clear';
                else if($request['alerts'][$i]['labels']['severity'] == 'none')
                    $input['severity'] = 'minor';
                else if($request['alerts'][$i]['labels']['severity'] == 'info')
                    $input['severity'] = 'minor';
                else if($request['alerts'][$i]['labels']['severity'] == 'warning')
                    $input['severity'] = 'major';
                else $input['severity'] = 'critical';
            }

            $input['region'] = 'BKK';
            
            $input['nodeIp'] = '';
            if(array_key_exists('instance',$request['alerts'][$i]['labels']))
                $input['nodeIp'] = $request['alerts'][$i]['labels']['instance'];

            $input['networkType'] = '';

            //Put the created set of parameters to webhook.log
            Log::channel('webhook')->info($input);

            //Send the parameters to Alarm API, get the response and put to webhook.log
            $param['json'] = $input;

            try{
                $response = $client->request('POST', '/FMGateway/rest/service/sendAlarm', $param);
                $code = $response->getStatusCode();
                $body = $response->getBody();

                Log::channel('webhook')->info($code);
                Log::channel('webhook')->info($body);
            }catch(Exception $e){
                Log::channel('webhook')->info($e);
            }
        }
        
        return response()->json([$request],200);
    }
}

